package com.devcamp.s50.car.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.car.api.model.CCar;
import com.devcamp.s50.car.api.model.CCarType;
import com.devcamp.s50.car.api.repository.ICarRepository;
import com.devcamp.s50.car.api.repository.ICarTypeRepository;

@RestController
@CrossOrigin
public class CarController {
    @Autowired
    ICarRepository carRepository;
    ICarTypeRepository carTypeRepository;

    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<CCar>> getCarList() {
        try {
            List<CCar> carList = new ArrayList<CCar>();
            carRepository.findAll().forEach(carList::add);
            return new ResponseEntity<>(carList, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CCarType>> getCarTypeList(@RequestParam(name = "carCode",required = true) String carCode) {
        try {
           CCar newCar = carRepository.findBycarCode(carCode);
           if (newCar!=null){
            return new ResponseEntity<>(newCar.getCarTypes(), HttpStatus.OK);
           } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
           }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
